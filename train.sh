export DATA_DIR=/net/big/guiguetv/datathon/
export EXP_NAME=b1

python run_classifier.py \
    --do_train \
    --do_eval \
    --data_dir $DATA_DIR/ \
    --bert_model bert-base-multilingual-cased \
    --max_seq_length 512 \
    --train_batch_size 16 \
    --learning_rate 2e-5 \
    --num_train_epochs 3.0 \
    --output_dir $DATA_DIR/output/$EXP_NAME \
    --gradient_accumulation_steps 2
