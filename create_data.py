import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('input_file')
parser.add_argument('output_dir')
parser.add_argument('--seed', default=2610)
parser.add_argument('--val_prop', default=0.2)
args = parser.parse_args()

np.random.seed(args.seed)

data = pd.read_csv(args.input_file, header=None, names=('_id', 'text', 'qid', 'label'))

true = data[data.label==True]
false = data[data.label==False]

print(f'There are {len(true)} True labels')
print(f'There are {len(false)} False labels')

# balance True/False labels
false_index = np.random.choice(false.index, size=len(true), replace=False)

false_to_keep = false.loc[false_index]

new_data = pd.concat([true, false_to_keep], axis=0)

# create train / val splits
val_index = np.random.choice(new_data.index, size=round(args.val_prop*len(new_data)), replace=False)
train_index = np.setdiff1d(new_data.index,val_index)
val_data = new_data.loc[val_index]
train_data = new_data.loc[train_index]

print('Number of val samples',val_data.shape)
print('Number of train samples',train_data.shape)

for _id in val_data._id:
    if _id in train_data._id:
        print('collusion')

n_true = len(val_data[val_data.label==True])

print(f'There are {n_true} True labels in val')

print('Saving...')
def replace_labels(data):
    data.loc[data.label==True, 'label'] = "1"
    data.loc[data.label==False, 'label'] = "0"
    return data
train_data = replace_labels(train_data)
val_data = replace_labels(val_data)

train_data.to_csv('train.csv',header=True, index=False)
val_data.to_csv('dev.csv', header=True, index=False)
