#!/usr/bin/env bash
# export DATA_DIR=/net/big/guiguetv/datathon
#export DATA_DIR = data
#export EXP_NAME = debug
python pretrain.py \
  --bert_model bert-base-multilingual-cased \
  --do_lower_case \
  --do_train \
  --train_file data/pretrain.txt \
  --output_dir models \
  --num_train_epochs 5.0 \
  --learning_rate 3e-5 \
  --train_batch_size 32 \
  --max_seq_length 128 \
